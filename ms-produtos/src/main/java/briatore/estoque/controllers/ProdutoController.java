package briatore.estoque.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import briatore.estoque.entities.Produto;
import briatore.estoque.services.ProdutoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@RestController
@RequestMapping("/produtos")
@Tag(name = "Produtos", description = "Microserviço para gerenciar produtos")
public class ProdutoController {
	private final ProdutoService produtoService;

	public ProdutoController(ProdutoService produtoService){
		this.produtoService = produtoService;
	}

	@GetMapping("/{id}/preco")
	@Operation(summary = "Consultar preço",
			description = "Retorna uma string com o preço de um produto")
	public ResponseEntity<String> getPreco(Long id) {
		String preco = produtoService.getPreco(id);
		return ResponseEntity.status(HttpStatus.OK).body(preco);
	}

	@GetMapping("/")
	@Operation(summary = "Listar produtos",
			description = "Retorna uma lista de produtos")
	public  ResponseEntity<List<Produto>> listar() {
		List<Produto> produtos =produtoService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(produtos);
	}

	@PostMapping("/")
	@Operation(summary = "Salvar produto",
			description = "Salva os dados do produto no banco de dados")
	public ResponseEntity<Produto> salvar(Produto produto) {
		Produto produtoSalvo = produtoService.salvar(produto);
		return ResponseEntity.status(HttpStatus.OK).body(produtoSalvo);
	}

	@PutMapping("/{id}")
	@Operation(summary = "Atualizar produto",
			description = "Atualiza os dados do produto no banco de dados")
	public ResponseEntity<Produto> atualizar(Produto produto) {
		Produto produtoAtualizado = produtoService.salvar(produto);
		return ResponseEntity.status(HttpStatus.OK).body(produtoAtualizado);
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Excluir produto",
			description = "Exclui os dados do produto no banco de dados")
	public ResponseEntity<String> excluir(Long id) {
		produtoService.excluir(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}