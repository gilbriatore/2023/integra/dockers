package briatore.estoque.controllers;

import briatore.estoque.entities.ItemEstoque;
import briatore.estoque.services.EstoqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estoque")
@Tag(name = "Estoque", description = "Microserviço para gerenciar estoque")
public class EstoqueController {

    private final EstoqueService controle;

    public EstoqueController(EstoqueService controle) {
        this.controle = controle;
    }

    @GetMapping("/itens")
    @Operation(summary = "Listar itens",
            description = "Retorna a listagem de itens em estoque")
    public List<ItemEstoque> listarItens(){
        return controle.listarItens();
    }

    @GetMapping("/itens/{id}")
    @Operation(summary = "Buscar item",
            description = "Retorna um item  específico do estoque")
    public ItemEstoque buscar(@PathVariable Long id){
        //return controle.listarItens().stream().filter(item -> item.getId().equals(id)).findFirst().get();
        ItemEstoque item = new ItemEstoque();
        return item;
    }

    @PostMapping("/itens")
    @Operation(summary = "Salvar item",
            description = "Faz a persistência de item em estoque")
    public ItemEstoque salvar(@RequestBody ItemEstoque item) {
        System.out.println(item);
        return controle.salvarItem(item);
    }

}
