package briatore.estoque.services;

import briatore.estoque.entities.ItemEstoque;
import briatore.estoque.entities.Produto;
import briatore.estoque.repositories.ItemEstoqueRepository;
import briatore.estoque.repositories.ProdutoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	private final ProdutoRepository produtoRepository;
	private final ItemEstoqueRepository itemRepository;

	public ProdutoServiceImpl(ProdutoRepository produtoRepository,
							  ItemEstoqueRepository itemEstoqueRepository) {
		this.produtoRepository = produtoRepository;
		this.itemRepository = itemEstoqueRepository;
	}
	@Override
	public String getPreco(Long id) {
		Optional<ItemEstoque> item = itemRepository.findById(id);
		BigDecimal preco = BigDecimal.ZERO;
		if (item.isPresent()) {
			preco = item.get().getPreco();
		}
		return preco.toString();
	}
	@Override
	public List<Produto> listar(){
		return produtoRepository.findAll();
	}

	@Override
	public Produto salvar(Produto produto) {
		return produtoRepository.save(produto);
	}

	@Override
	public Produto buscar(Long id) {
		Optional<Produto> produto = produtoRepository.findById(id);
		return produto.orElse(null);
	}

	@Override
	public void excluir(Long id){
		produtoRepository.deleteById(id);
	}
}
