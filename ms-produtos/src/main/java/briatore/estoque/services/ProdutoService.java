package briatore.estoque.services;

import briatore.estoque.entities.Produto;
import java.util.List;

public interface ProdutoService {
	String getPreco(Long id);
	List<Produto> listar();
	Produto buscar(Long id);
	Produto salvar(Produto produto);
	void excluir(Long id);
}
