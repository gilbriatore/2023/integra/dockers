package briatore.exemplos;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class P2_FilaDeTrabalho {

    private static final String FILA_DE_TRABALHO = "fila_de_trabalho";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.queueDeclare(FILA_DE_TRABALHO, true, false, false, null);

            for (int i = 0; i < 50000; i++) {
                String message = "Mensagem " + i;

                channel.basicPublish("", FILA_DE_TRABALHO,
                        MessageProperties.PERSISTENT_TEXT_PLAIN,
                        message.getBytes("UTF-8"));
                System.out.println(" [x] " + message + " enviada!");
            }
        }
    }

}
