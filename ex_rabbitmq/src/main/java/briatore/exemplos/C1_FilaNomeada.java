package briatore.exemplos;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import java.nio.charset.StandardCharsets;

public class C1_FilaNomeada {

    private final static String NOME_DA_FILA = "ola_rabbitmq";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(NOME_DA_FILA, false, false, false, null);
        System.out.println(" [*] Aguardando mensagens. Para sair pressione CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println(" [x] Recebido '" + message + "'");
        };
        channel.basicConsume(NOME_DA_FILA, true, deliverCallback, consumerTag -> { });
    }
}
