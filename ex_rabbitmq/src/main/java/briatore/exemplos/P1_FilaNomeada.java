package briatore.exemplos;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

public class P1_FilaNomeada {

    private final static String NOME_DA_FILA = "ola_rabbitmq";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.queueDeclare(NOME_DA_FILA, false, false, false, null);
            for (int i = 0; i < 100000; i++) {
                String message = "Olá RabbitMQ boy!!" + " " + i;
                channel.basicPublish("", NOME_DA_FILA, null, message.getBytes(StandardCharsets.UTF_8));
                System.out.println(" [x] Enviado '" + message + "'");
            }

        }
    }
}
