package briatore.exemplos;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class P3_PublicarAssinar {

    private static final String MEDIADOR = "meus_logs";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(MEDIADOR, BuiltinExchangeType.FANOUT);

            for (int i = 0; i < 1000000; i++) {
                String message = "Mensagem " + i;
                channel.basicPublish(MEDIADOR, "", null, message.getBytes("UTF-8"));
                System.out.println(" [x] " + message + " enviada!");
            }
        }
    }

}

