package briatore.entities;

public class Item {

	
	private Produto product;
	private Integer quantity;
	
	public Produto getProduct() {
		return product;
	}
	public void setProduct(Produto product) {
		this.product = product;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "[product:" + product + ", quantity:" + quantity + "]";
	}

}
 