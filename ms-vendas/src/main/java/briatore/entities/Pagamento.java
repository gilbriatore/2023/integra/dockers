package briatore.entities;
public class Pagamento {

	public enum Forma {
		DEBITO, CREDITO, PIX
	}
	public enum Status {
		AGUARDANDO, APROVADO, CANCELADO, CONCLUIDO, REPROVADO
	}

	private Forma forma;
	private String dados;
	private Status status;

	public boolean isAprovado() {
		return Status.APROVADO.equals(status);
	}

	public boolean isCancelado() {
		return Status.CANCELADO.equals(status);
	}


	public boolean isAguardando() {
		return Status.AGUARDANDO.equals(status);
	}
	public boolean isReprovado() {
		return Status.REPROVADO.equals(status);
	}

	public Forma getForma() {
		return forma;
	}
	public void setForma(Forma forma) {
		this.forma = forma;
	}
	public String getDados() {
		return dados;
	}
	public void setDados(String dados) {
		this.dados = dados;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}	
	
}
