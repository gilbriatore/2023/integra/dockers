package briatore.entities;

public class Produto {
	private Integer productId;
	private String productName;
	private float price;

	public Produto() {

	}

	public Produto(Integer productId, String productName, float price) {
		this.productId = productId;
		this.productName = productName;
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "[productId:" + productId + ", productName:" + productName + ", price:" + price + "]";
	}
	
}
