package briatore.entities;

import java.util.ArrayList;

public class Pedido {

	public enum Status {
		ABERTO, BAIXADO, CANCELADO, CONCLUIDO, RESERVADO, PAGTO_REPROVADO, PENDENTE_ESTOQUE, PENDENTE_PAGTO
	}

	private int orderId;
	private String date;

	private ArrayList<Item> items;
	private Pagamento pagamento;
	private Status status;
	
	@Override
	public String toString() {
		return "[orderId:" + orderId + ", orderCreationTime:" + date + ", orderProduct:"
				+ items + ", status:" + status + "]";
	}
	public Pedido(){
		
	}

	public boolean isPendenteDeEstoque() {
		return status == Status.PENDENTE_ESTOQUE;
	}

	public boolean isPendenteDePagamento() {
		return status == Status.PENDENTE_PAGTO;
	}

	public boolean isPagamentoReprovado() {
		return status == Status.PAGTO_REPROVADO;
	}

	public boolean isEstoqueReservado() {
		return status == Status.RESERVADO;
	}

	public boolean isConcluido() {
		return status == Status.CONCLUIDO;
	}

	public boolean isCancelado() {
		return status == Status.CANCELADO;
	}

	public boolean isAberto() {
		return status == Status.ABERTO;
	}

	public boolean isPendente() {
		return isPendenteDeEstoque() || isPendenteDePagamento();
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public ArrayList<Item> getItems() {
		return items;
	}
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Pagamento getPagamento() {
		return pagamento;
	}
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	
}
