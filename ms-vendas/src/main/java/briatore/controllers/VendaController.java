package briatore.controllers;

import briatore.entities.Pedido;
import briatore.services.VendaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Vendas", description = "Microserviço para gerenciar e processar vendas")
public class VendaController {
	private final VendaService vendaService;

	public VendaController(VendaService vendaService) {
		this.vendaService = vendaService;
	}

	@PostMapping("/finalizar")
	@Operation(summary = "Finalizar venda",
			description = "Faz a finalização de uma venda")
	public ResponseEntity<Pedido> finalizar(Pedido pedido) {
		pedido.setOrderId(0);

		Pedido pedidoProcessado = vendaService.finalizar(pedido);
		if (pedidoProcessado.isPendenteDeEstoque()){
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(pedidoProcessado);
		} else if (pedidoProcessado.isPagamentoReprovado()) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(pedidoProcessado);
		} else if (pedidoProcessado.isPendenteDePagamento()) {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(pedidoProcessado);
		}

		return ResponseEntity.status(HttpStatus.OK).body(pedidoProcessado);
	}

	@PostMapping("/cancelar")
	public ResponseEntity<Pedido> cancelar(Pedido pedido) {
		pedido.setOrderId(0);
		Pedido pedidoCancelado = vendaService.cancelar(pedido);
		return ResponseEntity.status(HttpStatus.OK).body(pedidoCancelado);
	}
}