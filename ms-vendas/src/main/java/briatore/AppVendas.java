package briatore;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info=@Info(title="Microserviço de vendas", version="1.0"))
public class AppVendas {

    public static void main(String[] args) {
        SpringApplication.run(AppVendas.class, args);
    }

}
