package briatore.services;

import briatore.entities.Pagamento;
import briatore.entities.Pedido;
import org.springframework.stereotype.Service;

@Service
public class VendaService {

	private final EstoqueService estoqueService;
	private final PagamentoService pagamentoService;

	public VendaService(EstoqueService estoqueService,
						PagamentoService paymentService) {
		this.estoqueService = estoqueService;
		this.pagamentoService = paymentService;
	}

	public Pedido finalizar(Pedido pedido) {
		pedido = estoqueService.reservar(pedido);
		if (pedido.isPendenteDeEstoque()) {
			return pedido;
		}
		Pagamento pagamento = pagamentoService.validar(pedido.getPagamento());
		if(pagamento.isReprovado()){
			pedido.setPagamento(pagamento);
			pedido.setStatus(Pedido.Status.PAGTO_REPROVADO);
			return pedido;
		}
		pagamento = pagamentoService.finalizar(pagamento);
		if (pagamento.isAguardando()) {
			pedido.setPagamento(pagamento);
			pedido.setStatus(Pedido.Status.PENDENTE_PAGTO);
			//Gravar o pedido no banco de dados
			return pedido;
		}
		pedido.setPagamento(pagamento);
		pedido.setStatus(Pedido.Status.CONCLUIDO);
		//Gravar o pedido no banco de dados
	    return pedido;
	}

	public Pedido cancelar(Pedido pedido) {
		Pagamento pagamento = pagamentoService.cancelar(pedido.getPagamento());
		pedido.setPagamento(pagamento);
		pedido.setStatus(Pedido.Status.CANCELADO);
		//Gravar alterações do pedido no banco de dados
		return pedido;
	}

}
