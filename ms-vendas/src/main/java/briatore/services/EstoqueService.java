package briatore.services;

import briatore.entities.Pagamento;
import briatore.entities.Pedido;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class EstoqueService {

	@Value("${pagamento-service.url}")
	String PAGAMENTO_SERVICE_URL;

	private final RestTemplate rest;

	public EstoqueService() {
		this.rest = new RestTemplate();
	}

	public Pedido reservar(Pedido pedido) {
		pedido.setStatus(Pedido.Status.RESERVADO);
		return pedido;
	}

	public Pedido liberar(Pedido pedido) {
		pedido.setStatus(Pedido.Status.CANCELADO);
		return pedido;
	}

	public Pedido baixar(Pedido pedido) {
		pedido.setStatus(Pedido.Status.CONCLUIDO);
		return pedido;
	}
}