package briatore.services;

import briatore.entities.Pagamento;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class PagamentoService {

	@Value("${pagamento-service.url}")
	String PAGAMENTO_SERVICE_URL;

	private final RestTemplate rest;

	public PagamentoService() {
		this.rest = new RestTemplate();
	}

	public Pagamento validar(Pagamento pagamento) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(List.of(MediaType.APPLICATION_JSON));
		HttpEntity<Pagamento> entity = new HttpEntity<>(pagamento,headers);
		ResponseEntity<Pagamento> response = rest.exchange(PAGAMENTO_SERVICE_URL + "/validar", HttpMethod.POST, entity, Pagamento.class);
		return response.getBody();
	}


	public Pagamento finalizar(Pagamento pagamento) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(List.of(MediaType.APPLICATION_JSON));
		HttpEntity<Pagamento> entity = new HttpEntity<>(pagamento,headers);
		ResponseEntity<Pagamento> response = rest.exchange(PAGAMENTO_SERVICE_URL + "/finalizar", HttpMethod.POST, entity, Pagamento.class);
		return response.getBody();
	}

	public Pagamento cancelar(Pagamento pagamento) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(List.of(MediaType.APPLICATION_JSON));
		HttpEntity<Pagamento> entity = new HttpEntity<>(pagamento,headers);
		ResponseEntity<Pagamento> response = rest.exchange(PAGAMENTO_SERVICE_URL + "/cancelar", HttpMethod.POST, entity, Pagamento.class);
		return response.getBody();
	}
}