# Dockers

Exemplos de microserviços que rodam bancos de dados MySql, PostgreSql, MongoDb e RabbitMQ utilizando docker compose.

## Baixar o projeto...

Fazer o download do zip ou clonar com:

```
git clone https://gitlab.com/gilbriatore/2023/integra/dockers.git
```

## Baixar o IntelliJ IDEA Community Edition...


- [ ] [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download)


Instalar a IDE, abrir os projetos e executar.