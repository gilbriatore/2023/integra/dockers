package briatore.controllers;

import briatore.entities.Pedido;
import briatore.services.PedidoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pedidos")
@Tag(name = "Pedidos", description = "Microserviço para gerenciar e processar pedidos")
public class PedidoController {

	private final PedidoService pedidoService;

	public PedidoController(PedidoService pedidoService) {
		this.pedidoService = pedidoService;
	}

	@GetMapping("/")
	@Operation(summary = "Listar pedidos",
			description = "Retorna uma lista de pedidos")
	public  ResponseEntity<List<Pedido>> listar() {
		List<Pedido> pedidos = pedidoService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(pedidos);
	}

	@PostMapping("/")
	@Operation(summary = "Salvar pedido",
			description = "Salva os dados do pedido no banco de dados")
	public ResponseEntity<Pedido> salvar(Pedido pedido) {
		Pedido pedidoSalvo = pedidoService.salvar(pedido);
		return ResponseEntity.status(HttpStatus.OK).body(pedidoSalvo);
	}

	@PutMapping("/{id}")
	@Operation(summary = "Atualizar pedido",
			description = "Atualiza os dados do pedido no banco de dados")
	public ResponseEntity<Pedido> atualizar(Pedido pedido) {
		Pedido pedidoAtualizado = pedidoService.salvar(pedido);
		return ResponseEntity.status(HttpStatus.OK).body(pedidoAtualizado);
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Excluir pedido",
			description = "Exclui os dados do pedido no banco de dados")
	public ResponseEntity<String> excluir(Long id) {
		pedidoService.excluir(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}