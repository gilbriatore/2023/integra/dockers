package briatore.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.math.BigDecimal;

@Entity
public class Pagamento {

	public enum Forma {
		DEBITO, CREDITO, PIX
	}
	public enum Status {
		ABERTO, APROVADO, CANCELADO, REPROVADO, CONCLUIDO
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Forma forma;
	private String responsavel;
	private String numeroCartao;
	private BigDecimal valor;
	private Status status;


	public Pagamento() {
	}

	public Pagamento(final String responsavel, final String numeroCartao, final BigDecimal valor) {
		this.responsavel = responsavel;
		this.numeroCartao = numeroCartao;
		this.status = Status.ABERTO;
		this.valor = valor;
	}

	public boolean isAprovado() {
		return Status.APROVADO.equals(status);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public void setValor(final BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValor() {
		return this.valor;
	}
	public boolean isCancelado() {
		return Status.CANCELADO.equals(status);
	}

	public boolean isReprovado() {
		return Status.REPROVADO.equals(status);
	}

	public Forma getForma() {
		return forma;
	}
	public void setForma(Forma forma) {
		this.forma = forma;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}	
	
}
