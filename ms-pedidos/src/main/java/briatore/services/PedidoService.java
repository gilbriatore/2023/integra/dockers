package briatore.services;

import briatore.entities.Pedido;

import java.util.List;

public interface PedidoService {
    List<Pedido> listar();
    Pedido buscar(Long id);
    Pedido salvar(Pedido produto);
    void excluir(Long id);
}
