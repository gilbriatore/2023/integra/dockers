package briatore.controllers;

import briatore.entities.Pagamento;
import briatore.services.PagamentoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.types.ObjectId;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pagamentos")
@Tag(name = "Pagamentos", description = "Microserviço para gerenciar e processar pagamentos")
public class PagamentoController {
	
	private final PagamentoService pagamentoService;

	public PagamentoController(PagamentoService pagamentoService){
		this.pagamentoService = pagamentoService;
	}

	@PostMapping("/validar")
	@Operation(summary = "Validar pagamento",
			description = "Faz a validação e aprovação de um pagamento")
	public ResponseEntity<Pagamento> validar(Pagamento pagamento) {
		pagamento.setStatus(Pagamento.Status.APROVADO);
		return ResponseEntity.status(HttpStatus.OK).body(pagamento);
	}

	@PostMapping("/processar")
	@Operation(summary = "Processar pagamento",
			description = "Faz o processamento de um pagamento")
	public ResponseEntity<Pagamento> processar(Pagamento pagamento) {
		pagamento.setStatus(Pagamento.Status.CONCLUIDO);
		return ResponseEntity.status(HttpStatus.OK).body(pagamento);
	}

	@PostMapping("/cancelar")
	@Operation(summary = "Cancelar pagamento",
			description = "Faz o cancelamento de um pagamento")
	public ResponseEntity<Pagamento> cancelar(Pagamento pagamento) {
		pagamento.setStatus(Pagamento.Status.CANCELADO);
		return ResponseEntity.status(HttpStatus.OK).body(pagamento);
	}

	@GetMapping("/")
	@Operation(summary = "Listar pagamentos",
			description = "Retorna uma lista de pagamentos")
	public  ResponseEntity<List<Pagamento>> listar() {
		List<Pagamento> pagamentos = pagamentoService.listar();
		return ResponseEntity.status(HttpStatus.OK).body(pagamentos);
	}

	@PostMapping("/")
	@Operation(summary = "Salvar pagamento",
			description = "Salva os dados do pagamento no banco de dados")
	public ResponseEntity<Pagamento> salvar(Pagamento pagamento) {
		Pagamento pagamentoSalvo = pagamentoService.salvar(pagamento);
		return ResponseEntity.status(HttpStatus.OK).body(pagamentoSalvo);
	}

	@PutMapping("/{id}")
	@Operation(summary = "Atualizar pagamento",
			description = "Atualiza os dados do pagamento no banco de dados")
	public ResponseEntity<Pagamento> atualizar(Pagamento pagamento) {
		Pagamento pagamentoAtualizado = pagamentoService.salvar(pagamento);
		return ResponseEntity.status(HttpStatus.OK).body(pagamentoAtualizado);
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Excluir pagamento",
			description = "Exclui os dados do pagamento no banco de dados")
	public ResponseEntity<String> excluir(ObjectId id) {
		pagamentoService.excluir(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
}
