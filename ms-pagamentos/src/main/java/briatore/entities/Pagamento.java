package briatore.entities;

import io.swagger.v3.oas.annotations.Hidden;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pagamentos")
public class Pagamento extends AbstractModel{

	public enum Forma {
		DEBITO, CREDITO, PIX
	}
	public enum Status {
		APROVADO, CANCELADO, REPROVADO, CONCLUIDO
	}

	@Id
	private ObjectId id;
	private Forma forma;
	@Indexed
	private String dados;
	@Indexed
	private Status status;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	@Hidden
	public boolean isAprovado() {
		return Status.APROVADO.equals(status);
	}
	@Hidden
	public boolean isCancelado() {
		return Status.CANCELADO.equals(status);
	}

	@Hidden
	public boolean isReprovado() {
		return Status.REPROVADO.equals(status);
	}
	
	public Forma getForma() {
		return forma;
	}
	public void setForma(Forma forma) {
		this.forma = forma;
	}
	public String getDados() {
		return dados;
	}
	public void setDados(String dados) {
		this.dados = dados;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}	
	
}
