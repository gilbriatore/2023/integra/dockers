package briatore.services;

import briatore.entities.Pagamento;
import briatore.repositories.PagamentoRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoServiceImpl implements PagamentoService {

	private final PagamentoRepository pagamentoRepository;

	public PagamentoServiceImpl(PagamentoRepository produtoRepository) {
		this.pagamentoRepository = produtoRepository;
	}
	@Override
	public List<Pagamento> listar(){
		return pagamentoRepository.findAll();
	}

	@Override
	public Pagamento salvar(Pagamento produto) {
		return pagamentoRepository.save(produto);
	}

	@Override
	public Pagamento buscar(ObjectId id) {
		Optional<Pagamento> produto = pagamentoRepository.findById(id);
		return produto.orElse(null);
	}

	@Override
	public void excluir(ObjectId id){
		pagamentoRepository.deleteById(id);
	}
}
