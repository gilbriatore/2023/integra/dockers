package briatore.services;

import briatore.entities.Pagamento;
import org.bson.types.ObjectId;

import java.util.List;

public interface PagamentoService {
	List<Pagamento> listar();
	Pagamento buscar(ObjectId id);
	Pagamento salvar(Pagamento pagamento);
	void excluir(ObjectId id);
}
